import FormData from "form-data";
import axios from "axios";

export default async function storeImage(image) {
  if (!image) return "";
  if (typeof image === "string") return image;

  let formData = new FormData();
  formData.append("file", image, image.name);

  let filename;

  try {
    const res = await axios.post("/api/image", formData, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
    return res.data.filename;
  } catch (e) {
    console.log(e);
  }

  return "";
}
