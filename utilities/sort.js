import config from "../papperlaweb.config";

export default function sort(beers) {
  const categories = config.categories.map((c) => c.name);

  const compare_func = (a, b) => {
    /*
     * first, sort by category
     * second, sort by price
     */

    const indices = [
      categories.indexOf(a.category),
      categories.indexOf(b.category),
    ];

    if (indices[0] == -1) return 1;
    if (indices[1] == -1) return -1;

    if (indices[0] < indices[1]) return -1;
    if (indices[0] > indices[1]) return 1;

    if (a.price < b.price) return -1;
    if (a.price > b.price) return 1;
    return 0;
  };

  beers.sort(compare_func);
}
