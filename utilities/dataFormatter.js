import config from "../papperlaweb.config";

export function formatPrice(price) {
  if (price % 1 === 0) {
    return price + ".-";
  }
  return price.toFixed(2);
}

export function formatDate(dateStr) {
  const date = new Date(Number(dateStr));
  return (
    ("0" + date.getDate()).slice(-2) +
    "." +
    ("0" + (date.getMonth() + 1)).slice(-2) +
    "." +
    date.getFullYear()
  );
}

export function getColor(category) {
  return config.categories.filter((c) => c.name == category)[0].menuColor;
}

export function getBadgeColor(category) {
  return config.categories.filter((c) => c.name == category)[0].badgeColor;
}

export function formatIngredients(ingredients) {
  return ingredients
    .map((ing) => {
      return ing.ingredient.name;
    })
    .join(", ");
}
