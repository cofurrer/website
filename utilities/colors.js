export function getTextColor(theme) {
  return theme.colorScheme == "dark" ? "#C1C2C5" : "black";
}

export function getAccentColor(theme) {
  return theme.colorScheme == "dark" ? "#C1C2C5" : "#4f2a17";
}

export function getDisabledColor() {
  return "#777777";
}

export function getIconColor(theme) {
  return theme.colorScheme == "dark" ? "white" : "#4f2a17";
}
