This is the Website of PapperlaPub, originally written by [Andreas Hirsch](mailto:hirscha@student.ethz.ch) and [Alexander Schoch](mailto:alexander.schoch@bluewin.ch) in Next.js. If you encounter any problems, just hit us up.

[[_TOC_]]

# Configuration

The file `papperlaweb.config.js` is a configuration file for this application. All options in this file *are supposed to be changed by the PapperlaPub board* at any time. To update the configuration, edit this file and create a merge request to the project. After merging, the automatic build and deployment, the changes should be online within 15 minutes after merge.

# Development Setup

## Prerequisites

In order to run this application, you'll need to have the following software installed on your system:
- `nodejs` 18
- `npm`
- a `mariadb` or `mysql` server (also possible remotely or in docker container)

In order to get the specific node version needed, it is easiest to install [nvm](https://github.com/nvm-sh/nvm) and run

```
nvm install 18
nvm use 18
```

to get node 18. Whenever you get weird syntax errors when running the application, make sure that you are on node 18.

## Configuration

Next, download the source code _via_

```
git clone https://gitlab.ethz.ch/vseth/0500-kom/0516-pub/sip-pub-apps/website
cd website
```

and install all JavaScript packages _via_

```
npm install
```

Next, you'll need to configure some environment variables. For this, create a file `.env` and add the following text:

```
DATABASE_URL="mysql://[username]:[password]@[host]:[port]/[dbname]?schema=public"

SIP_AUTH_PAPPERLAWEB_CLIENT_ID=
SIP_AUTH_PAPPERLAWEB_CLIENT_SECRET=
SIP_AUTH_OIDC_ISSUER=

NEXTAUTH_URL=http://localhost:3000
NEXTAUTH_SECRET=just_any_text

MAILER_HOST=mail.ethz.ch
MAILER_NAME=[display_name]
MAILER_USERNAME=[username]
MAILER_PASSWORD=[password]

SIP_S3_PAPPERLAWEB_ACCESS_KEY=
SIP_S3_PAPPERLAWEB_BUCKET=
SIP_S3_PAPPERLAWEB_SECRET_KEY=
SIP_S3_PAPPERLAWEB_USE_SSL=true
SIP_S3_PAPPERLAWEB_HOST=minio-redzone.vseth.ethz.ch
SIP_S3_PAPPERLAWEB_PORT=443
```

Here, the `DATABASE_URL` defines the database connection, `SIP_AUTH_[...]` is the connection the VSETH keycloak for user authentication (and authorization), `NEXTAUTH_[...]` are used for next-auth and have to be set for security reasons, `MAILER_[...]` are mail account credentials for the newsletter and `SIP_S3_[...]` are credentials for the S3 bucket for image storage.

The application does still work if some of these variables are unset, but the specific feature they're for won't work.

## Database Setup

In order to setup the database, run the following command:

```
npx prisma migrate dev --name init
```

This will create the database along with its schema.

## Development Server

Last, run 

```
npm run dev
```

to start the development server at `localhost:3000`. 

# Overall Architecture

The appliation is written in Next.js *without* TypeScript for Maintenance reasons.

Starting from the frontend, all components are written in React using `Mantine-UI` and some `VSETH-canine` components (but pre-rendered by the Next.js server). Whenever data is fetched/sent, a query/migration is sent to `/api/graphql` using the `@apollo/client` package. On the backend, a Next.js api route handles these requests via various resolvers (defined in `graphql/resolvers.js`). These resolvers use `prisma` to communicate with the database.

The only exception to this is the api endpoint `/api/image` for uploading images to S3, as graphql is only suited for json.

# API

Most actions can be done _via_ the user interface. However, for automated maintenace, testing and some dangerous features (e.g. deletion of entries), the API can be accessed directly. For this, visit the `/api/graphql` endpoint (e.g. https://papperlapub.ethz.ch/api/graphql) for the Yoga GraphiQL interface.

An overview over all resolvers and the schema can be viewed by clicking the "Show Documentation Explorer" on the top left.

## Queries

Queries are operations that only fetch data and write nothing. For almost all queries, no user authentication is needed. A query can, for example, look like so:

```
query getBeers {
  getBeers {
    name
    brewery
    type
  }
}
```

Here, the `query` keyword defines the type of request, the first `getBeers` gives the query a name in Yoga (but is optional), the second `getBeers` specifies which resolver should be used, and the fields within `{}` specify which data should be returned by the server. Note that graphql only returns data that is specified here, even if there are more fields on an entity.

The query above might return something like this:

```
{
  "data": {
    "getBeers": [
      {
        "name": "Lager",
        "brewery": "Augustiner",
        "type": "Lager"
      },
      {
        "name": "L'Abbaye de Saint Bon Chien",
        "brewery": "BFM",
        "type": "Sour"
      }
    ]
  }
}
```

Some entities have other entities as a field. This is how to access those:

```
query getCocktails {
  getCocktails {
    name
    tags {
      tag {
        name
      }
    }
  }
}
```

This query might return this:

```
{
  "data": {
    "getCocktails": [
      {
        "name": "Gin Tonic",
        "ingredients": [
          {
            "ingredient": {
              "name": "London Dry Gin"
            }
          },
          {
            "ingredient": {
              "name": "Tonic Water"
            }
          }
        ]
      },
      {
        "name": "Martini",
        "ingredients": [
          {
            "ingredient": {
              "name": "London Dry Gin"
            }
          },
          {
            "ingredient": {
              "name": "Sweet Vermouth"
            }
          }
        ]
      }
    ]
  }
}
```

## Mutations

Mutations are requests that will change data. They pretty much always need some parameters to control what is changed.

A mutation might look like this:

```
mutation updateAvailability {
  updateAvailability(id:10,status:false)
}
```

This specific resolver needs the id from the beer that should be updated, and the status of this beer, i.e. if it is available after the mutation. The mutation as run above would change the availability from beer with id 10 to unavailable.

The mutation above might return this:

```
{
  "data": {
    "updateAvailability": true
  }
}
```

True usually means that this change was successful.

If a mutation returns a primitive data type, no `{}` after the parameters are necessary. If it returns an object, you'll have to specify which fields to fetch.

The following mutation will delete a beer:

```
mutation deleteBeer {
  deleteBeer(id:10)
}
```

# Code Structure

The code contains the following folders:

- `components`: Components that are used in other comopnents or pages are found here. One file is one component.
- `graphql`: Everything graphql related is in here. `graphql/schema.js` defines the data schema and `graphql/resovlers.js` defines what each resolver does.
- `pages`: This folder mimics the strucutre of the application itself. Thus, e.g. the blog page is found in `pages/blog.jsx`. These pages might load more components if necessary. 
- `pages/api`: These are the Next.js api routes and are not sent to the client.
- `prisma`: Everything prisma related is in here. Most importantly, `prisma/schema.prisma` defines the database schema.
- `public`: Static files, e.g. Logos or fonts, are found here.
- `styles`: Contains the global css file.
- `utilities`: Some useful functions, e.g. access checking or color calculations, are found here.

Imoprtant files:

- `Dockerfile`: Build instructions for the docker container
- `cinit.yml`: The VSETH cluster uses cinit for starting up applications. This file defines this process.
- `papperlaweb.config.js`: In this file, the PapperlaPub board can change certain parameters on their own. All options are documented in the file itself.
- `sip.yml`: This file defines all services from the VSETH SIP this application needs.
