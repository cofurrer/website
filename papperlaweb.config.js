const config = {
  /*
   * Beer categories, visible on all Beer Cards and color coded in the menu.
   *
   * Name: Displayed when adding/editing a beer and in the badge next to
   *   the beer name
   * badgeColor: Color of the badge next to the beer name.
   *   Available are: https://mantine.dev/core/badge/
   * menuColor: Hex code or CSS name for color shown on the menu
   */
  categories: [
    {
      name: "standard",
      badgeColor: "gray",
      menuColor: "white",
    },
    {
      name: "alcohol-free",
      badgeColor: "green",
      menuColor: "#00f200",
    },
    {
      name: "seasonal",
      badgeColor: "yellow",
      menuColor: "#fff200",
    },
    {
      name: "special",
      badgeColor: "red",
      menuColor: "#ff0000",
    },
    {
      name: "ultra rare",
      badgeColor: "grape",
      menuColor: "#992c99",
    },
  ],
  /*
   * The menu should update the data every n seconds.
   */
  refetchFrequency: 2,
  /*
   * Display available cocktails in menu.
   */
  showCocktailsInMenu: true,
  /*
   * Display available spirits in menu.
   */
  showSpiritsInMenu: true,
  /*
   * Display the "2CHF für Gläser und Flaschen" text in menu
   */
  showDepotText: false,
  /*
   * If people click on 'reply' in a newsletter, where should it go?
   */
  mailReplyTo: "vorstand@papperlapub.ethz.ch",
  /*
   * How many blog posts should be displayed at once?
   */
  blogPostCountPerPage: 5,
  /*
   * Texts for the main header, in english and german.
   */
  mainHeaderTexts: [
    {
      de: "Wir sind jeden Mittwoch ab 18:00 Uhr im CAB D 21 für euch da und versüssen euch den Abend mit kühlem Bier, leckeren Drinks und coolen Specials.",
      en: "We are here for you every Wednesday from 6pm at CAB D 21 and sweeten your evening with cool beer, tasty drinks and cool specials.",
    },
    {
      de: "Kontakt: <a href='mailto:vorstand@papperlapub.ethz.ch'>vorstand@papperlapub.ethz.ch</a>",
      en: "Contact Us: <a href='mailto:vorstand@papperlapub.ethz.ch'>vorstand@papperlapub.ethz.ch</a>",
    },
    {
      de: "<a target='_blank' href='https://lists.papperlapub.ethz.ch/sympa/subscribe/newsletter-list'>Abonniere unseren Newsletter!</a>",
      en: "<a target='_blank' href='https://lists.papperlapub.ethz.ch/sympa/subscribe/newsletter-list'>Subscribe to our newsletter!</a>",
    },
  ],
  /*
   * Texts for /about, in english and german. Can interpret html tags.
   */
  aboutTexts: [
    {
      de: {
        title: "Wann und Wo?",
        text: "Wir sind jeden Mittwoch ab 18:00 Uhr im CAB D 21 für euch da und versüssen euch den Abend mit kühlem Bier, leckeren Drinks und coolen Specials.",
      },
      en: {
        title: "When and where?",
        text: "We are here for you every Wednesday from 6pm at CAB D 21 and sweeten your evening with cool beer, tasty drinks and cool specials.",
      },
    },
    {
      de: {
        title: "Newsletter",
        text: 'Abonniere gerne unseren Newsletter unter <a target="_blank" href="https://lists.papperlapub.ethz.ch/sympa/subscribe/newsletter-list">https://lists.papperlapub.ethz.ch/sympa/subscribe/newsletter-list.</a>',
      },
      en: {
        title: "Newsletter",
        text: 'Please subscribe to our newsletter at <a target="_blank" href="https://lists.papperlapub.ethz.ch/sympa/subscribe/newsletter-list">https://lists.papperlapub.ethz.ch/sympa/subscribe/newsletter-list.</a>',
      },
    },
    {
      de: {
        title: "Instagram",
        text: 'Für Eindrücke aus und rund um das PapperlaPub folge doch unserer  Instagrampage. Du findest uns dort unter <a target="_blank" href="https://instagram.com/papperlapinsta">@papperlapinsta</a>',
      },
      en: {
        title: "Instagram",
        text: 'For impressions from and around the PapperlaPub, follow our Instagram page. You can find us there under <a target="_blank" href="https://instagram.com/papperlapinsta">@papperlapinsta</a>',
      },
    },
    {
      de: {
        title: "Du hast Lust mitzuhelfen?",
        text: 'Super! Wir sind immer auf der Suche nach motivierter Unterstützung für unser Team. Melde dich am besten per Mail an <a target="_blank" href="mailto:vorstand@papperlapub.ethz.ch">vorstand@papperlapub.ethz.ch</a>, per Brief an die Anschrift links, oder komm einfach am nächsten Mittwoch bei uns vorbei. Zwingend erforderlich ist, egal welchen Weg du wählst, lediglich deine Kompetenz.',
      },
      en: {
        title: "You want to help out?",
        text: 'Awesome! We are always looking for motivated support for our team. The best way to get in touch is to send an email to <a target="_blank" href="mailto:vorstand@papperlapub.ethz.ch">vorstand@papperlapub.ethz.ch</a>, a letter to the address on the left, or simply drop by next Wednesday. No matter which way you choose, all that is required is your competence.',
      },
    },
    {
      de: {
        title: "Du hast eine Idee?",
        text: 'Geht es etwa um eine super Idee für ein neues Special? Oder um sonstiges konstruktives Feedback? Dann melde dich einfach mal bei uns per Mail an <a target="_blank" href="mailto:vorstand@papperlapub.ethz.ch">vorstand@papperlapub.ethz.ch</a> oder schreibe einen Brief, oder komme einfach an einem Mittwoch deiner Wahl bei uns vorbei, wir sind eh da!',
      },
      en: {
        title: "You have an idea?",
        text: 'Is it about a great idea for a new special? Or any other constructive feedback? Then simply contact us by email at <a target="_blank" href="mailto:vorstand@papperlapub.ethz.ch">vorstand@papperlapub.ethz.ch</a> or write a letter, or just drop by on a Wednesday of your choice, we\'ll be there anyway!',
      },
    },
    {
      de: {
        title: "Interesse an einem Hosting?",
        text: "Bist du von einer anderen VSETH-Kommission oder Organisation und möchstest bei uns ein Hosting organisieren? Dann kontaktiere uns unter <a href='mailto:hofnarr@papperlapub.ethz.ch'>hofnarr@papperlapub.ethz.ch</a>.",
      },
      en: {
        title: "Interested in a hosting?",
        text: "Are you from another VSETH committee or a student association and want to organize a hosting with us? Then contact us at <a href='mailto:hofnarr@papperlapub.ethz.ch'>hofnarr@papperlapub.ethz.ch</a>.",
      },
    },
  ],
};

export default config;
