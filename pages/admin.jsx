import { useState } from "react";

import { Button, Container, Group } from "@mantine/core";

import AddBeerModal from "../components/addBeerModal";

function Admin() {
  const [addBeer, setAddBeer] = useState(false);

  return (
    <Container size="xl">
      <div style={{ textAlign: "center" }}>
        <h1>Admin</h1>
      </div>

      <AddBeerModal open={addBeer} close={() => setAddBeer(false)} />
    </Container>
  );
}

export default Admin;
