import { useRouter } from "next/router";

import { Container } from "@mantine/core";

import parse from "html-react-parser";

import { serverSideTranslations } from "next-i18next/serverSideTranslations";

import config from "../papperlaweb.config";

function About() {
  const { locale } = useRouter();

  return (
    <Container size="xl" style={{ width: "100%" }}>
      {config.aboutTexts.map((entry, i) => (
        <div key={i}>
          <h2 style={{ marginBottom: 0, marginTop: "10px" }}>
            {parse(entry[locale || "en"].title)}
          </h2>
          <p>{parse(entry[locale || "en"].text)}</p>
        </div>
      ))}
    </Container>
  );
}

export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      protected: false,
    },
  };
}

export default About;
