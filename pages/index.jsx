import { useRouter } from "next/router";

import BeerMenu from "../components/beerMenu";
import CocktailMenu from "../components/cocktailMenu";
import SpiritMenu from "../components/spiritMenu";
import MainHeader from "../components/mainHeader";

import { Container, Space } from "@mantine/core";

import { serverSideTranslations } from "next-i18next/serverSideTranslations";

import parse from "html-react-parser";

import config from "../papperlaweb.config";

import { gql, useQuery } from "@apollo/client";

const getBeersQuery = gql`
  {
    getBeers {
      id
      name
      type
      brewery
      price
      volume
      category
      isAvailable
      isArchived
      alcohol
      onTap
      menuOverride
      comments {
        id
        comment
        submitter {
          name
        }
        date
      }
      ratings {
        rating
        user {
          sub
        }
      }
    }
  }
`;

const getCocktailsQuery = gql`
  {
    getCocktails {
      id
      name
      price
      containsAlcohol
      ingredients {
        ingredient {
          name
          isAvailable
        }
      }
      tags {
        tag {
          name
        }
      }
      isArchived
      imagePortrait
      imageLandscape
    }
  }
`;

const getSpiritsQuery = gql`
  {
    getSpirits {
      id
      name
      type
      price
      alcohol
      tags {
        tag {
          name
        }
      }
      isAvailable
      isArchived
    }
  }
`;

export default function Index() {
  const { data: beers, refetch: refetchB } = useQuery(getBeersQuery);
  const { data: cocktails, refetch: refetchC } = useQuery(getCocktailsQuery);
  const { data: spirits, refetch: refetchS } = useQuery(getSpiritsQuery);

  const { locale } = useRouter();

  return (
    <Container size="xl" style={{ width: "100%" }}>
      <MainHeader />
      <section id="beers">
        <BeerMenu
          beers={beers?.getBeers}
          refetch={refetchB}
          showButtons={true}
        />
      </section>
      <CocktailMenu cocktails={cocktails?.getCocktails} refetch={refetchC} />
      <SpiritMenu spirits={spirits?.getSpirits} refetch={refetchS} />

      <Space h="xl" />
    </Container>
  );
}

export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      protected: false,
    },
  };
}
