import { Container } from "@mantine/core";

import { useTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

import parse from "html-react-parser";

function Unauthorized() {
  const { t } = useTranslation("common");
  return (
    <Container size="xl">
      <h1 style={{ margin: 0 }}>{t("error")}</h1>
      <h2 style={{ marginTop: 0 }}>401: Unauthorized</h2>

      <p>{parse(t("401"))}</p>
    </Container>
  );
}

export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      protected: false,
    },
  };
}

export default Unauthorized;
