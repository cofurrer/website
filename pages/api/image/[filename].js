import fs from "fs";
import path from "path";
import { Formidable } from "formidable";
import { v4 as uuidv4 } from "uuid";

export const config = {
  api: {
    bodyParser: false,
  },
};

export default async function handler(req, res) {
  if (req.method === "GET") {
    const filename = req.query.filename;
    if (!filename) res.status(404).json({ error: "File not Found" });

    const filePath = path.join(process.cwd(), `/public/images/${filename}`);
    const extension = filename.split(".").pop();

    const imageBuffer = fs.readFileSync(filePath);
    res.setHeader("Content-Type", "image/" + extension);
    res.send(imageBuffer);
  } else {
    res.status(405).json({ error: "Method not allowed" });
  }
}
