import multiparty from "multiparty";
import formidable from "formidable";
import { Upload } from "@aws-sdk/lib-storage";
import { S3 } from "@aws-sdk/client-s3";
import { Transform } from "stream";

export const config = {
  api: {
    bodyParser: false, // Disable built-in body parsing
  },
};

const s3 = new S3({
  credentials: {
    accessKeyId: process.env.SIP_S3_PAPPERLAWEB_ACCESS_KEY,
    secretAccessKey: process.env.SIP_S3_PAPPERLAWEB_SECRET_KEY,
  },
  endpoint: "https://" + process.env.SIP_S3_PAPPERLAWEB_HOST,
  region: "eu-central-2",
});

export default async function handler(req, res) {
  if (req.method === "POST") {
    const url = await new Promise((resolve, reject) => {
      const form = formidable();

      form.parse(req, (err, fields, files) => {});

      form.on("error", (error) => {
        reject(error.message);
      });

      form.on("data", (data) => {
        if (data.name === "complete") {
          resolve(data.filename);
        }
      });

      form.on("fileBegin", (formName, file) => {
        file.open = async function () {
          this._writeStream = new Transform({
            transform(chunk, encoding, callback) {
              callback(null, chunk);
            },
          });

          this._writeStream.on("error", (e) => {
            form.emit("error", e);
          });

          const newFilename = `${Date.now()}_${this.originalFilename}`;
          const imageUrl =
            "https://" +
            process.env.SIP_S3_PAPPERLAWEB_HOST +
            "/" +
            process.env.SIP_S3_PAPPERLAWEB_BUCKET +
            "/images/" +
            newFilename;

          const s3Params = {
            Bucket: process.env.SIP_S3_PAPPERLAWEB_BUCKET,
            Key: `images/${newFilename}`,
            ACL: "public-read",
            Body: this._writeStream,
          };
          const uploadResult = await new Upload({
            client: s3,
            params: s3Params,
          })
            .done()
            .then((data) => {
              form.emit("data", {
                name: "complete",
                value: data,
                filename: imageUrl,
              });
            })
            .catch((err) => {
              form.emit("error", err);
            });
        };
      });
    });
    return res.status(200).json({ status: "ok", filename: url });
  } else {
    res.status(405).json({ error: "Method not allowed" });
  }
}
