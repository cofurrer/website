import { Container } from "@mantine/core";

import { useTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

import parse from "html-react-parser";

function NotFound() {
  const { t } = useTranslation("common");
  return (
    <Container size="xl">
      <h1 style={{ margin: 0 }}>{t("error")}</h1>
      <h2 style={{ marginTop: 0 }}>404: Page not Found</h2>

      <p>{parse(t("404"))}</p>
    </Container>
  );
}

export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      protected: false,
    },
  };
}

export default NotFound;
