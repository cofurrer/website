import prisma from "../lib/prisma";
import { getServerSession } from "next-auth/next";
import { authOptions } from "../pages/api/auth/[...nextauth]";

import config from "../papperlaweb.config";
import hasAccess from "../utilities/hasAccess";
import sort from "../utilities/sort";
import isAvailable from "../utilities/isAvailable";

import nodemailer from "nodemailer";

export const resolvers = {
  Query: {
    getBeers: async () => {
      const beers = await prisma.beer.findMany({
        where: { isArchived: false },
        orderBy: [{ name: "asc" }],
        include: {
          ratings: {
            include: {
              user: {
                select: {
                  sub: true,
                  name: false,
                },
              },
            },
          },
        },
      });
      sort(beers);
      return beers;
    },
    getAvailableBeers: async () => {
      const beers = await prisma.beer.findMany({
        where: { isArchived: false, isAvailable: true },
        orderBy: [{ name: "asc" }],
      });
      sort(beers);
      return beers;
    },
    getBeerById: async (_, { id }) => {
      const beer = prisma.beer.findUnique({
        where: { id },
        include: {
          comments: {
            include: {
              submitter: {
                select: {
                  name: true,
                  sub: false,
                },
              },
            },
          },
          ratings: {
            include: {
              user: {
                select: {
                  sub: true,
                  name: false,
                },
              },
            },
          },
          flavours: {
            include: {
              user: {
                select: {
                  sub: true,
                  name: false,
                },
              },
            },
          },
        },
      });
      return beer;
    },
    getArchivedBeers: async () => {
      const beers = await prisma.beer.findMany({
        where: { isArchived: true },
        orderBy: [{ name: "asc" }],
        include: {
          ratings: {
            include: {
              user: {
                select: {
                  sub: true,
                  name: false,
                },
              },
            },
          },
        },
      });
      sort(beers);
      return beers;
    },
    getCocktails: async () => {
      const cocktails = await prisma.cocktail.findMany({
        include: {
          ingredients: {
            include: {
              ingredient: true,
            },
          },
          tags: {
            include: {
              tag: true,
            },
          },
        },
        where: { isArchived: false },
        orderBy: [{ name: "asc" }],
      });
      return cocktails;
    },
    getAvailableCocktails: async () => {
      const cocktails = await prisma.cocktail.findMany({
        include: {
          ingredients: {
            include: {
              ingredient: true,
            },
          },
          tags: {
            include: {
              tag: true,
            },
          },
        },
        where: { isArchived: false },
        orderBy: [{ name: "asc" }],
      });
      return cocktails.filter((cocktail) => isAvailable(cocktail));
    },
    getArchivedCocktails: async () => {
      const cocktails = await prisma.cocktail.findMany({
        include: {
          ingredients: {
            include: {
              ingredient: true,
            },
          },
          tags: {
            include: {
              tag: true,
            },
          },
        },
        where: { isArchived: true },
        orderBy: [{ name: "asc" }],
      });
      return cocktails;
    },
    getSpirits: async () => {
      const spirits = await prisma.spirit.findMany({
        where: {
          isArchived: false,
        },
        include: {
          tags: {
            include: {
              tag: true,
            },
          },
        },
        orderBy: [{ name: "asc" }],
      });
      return spirits;
    },
    getAvailableSpirits: async () => {
      const spirits = await prisma.spirit.findMany({
        where: {
          isArchived: false,
          isAvailable: true,
        },
        orderBy: [{ name: "asc" }],
      });
      return spirits;
    },
    getArchivedSpirits: async () => {
      const spirits = await prisma.spirit.findMany({
        where: {
          isArchived: true,
        },
        include: {
          tags: {
            include: {
              tag: true,
            },
          },
        },
        orderBy: [{ name: "asc" }],
      });
      return spirits;
    },
    getIngredients: async () => {
      const ingredients = await prisma.ingredient.findMany({
        orderBy: [{ name: "asc" }],
      });
      return ingredients;
    },
    getTags: async () => {
      const tags = await prisma.tag.findMany({
        orderBy: [{ name: "asc" }],
      });
      return tags;
    },
    getPosts: async (_, { skip, take }) => {
      const posts = await prisma.post.findMany({
        orderBy: [{ date: "desc" }],
        skip,
        take,
      });
      return posts;
    },
    getNewestPost: async () => {
      const post = await prisma.post.findFirst({
        orderBy: [{ date: "desc" }],
      });
      return post;
    },
    getPostCount: async () => {
      const count = await prisma.post.count();
      return count;
    },
    export: async (_, { name, brewery }, { session }) => {
      if (!hasAccess(session, true)) return false;

      const beer = await prisma.beer.findFirst({
        where: {
          name,
          brewery,
        },
      });

      const str = [
        beer.menuOverride || beer.brewery + " " + beer.name,
        " ",
        beer.type,
        " ",
        " ",
        beer.onTap ? "G." + beer.volume : "?." + beer.volume,
        " ",
        "Fr. " + beer.price.toFixed(2),
        " ",
        " ",
        " ",
        beer.alcohol.toFixed(1),
      ].join(";");
      return str;
    },
    getComments: async (_, { beerId }) => {
      const comments = prisma.comment.findMany({
        where: {
          beer: {
            id: beerId,
          },
        },
      });

      return comments;
    },
  },
  Mutation: {
    addBeer: async (_, data, { session }) => {
      if (!hasAccess(session, true)) return false;

      const beer = await prisma.beer.create({
        data: {
          ...data,
        },
      });
      return beer ? true : false;
    },
    deleteBeer: async (_, { id }, { session }) => {
      if (!hasAccess(session, true)) return false;

      await prisma.beer.delete({ where: { id } });
      return true;
    },
    editBeer: async (_, data, { session }) => {
      if (!hasAccess(session, true)) return false;

      const res = await prisma.beer.update({
        where: { id: data.id },
        data: {
          ...data,
        },
      });
      return res ? true : false;
    },
    updateAvailability: async (_, { id, status }, { session }) => {
      if (!hasAccess(session, true)) return false;

      const res = await prisma.beer.update({
        where: { id },
        data: {
          isAvailable: status,
        },
      });
      return res ? true : false;
    },
    addCocktail: async (_, data, { session }) => {
      if (!hasAccess(session, true)) return false;

      const cocktail = await prisma.cocktail.create({
        data: {
          ...data,
          ingredients: {
            create: data.ingredients.map((ingredient) => ({
              ingredient: {
                connectOrCreate: {
                  where: {
                    name: ingredient,
                  },
                  create: {
                    name: ingredient,
                    isAvailable: false,
                  },
                },
              },
            })),
          },
          tags: {
            create: data.tags.map((tag) => ({
              tag: {
                connectOrCreate: {
                  where: {
                    name: tag,
                  },
                  create: {
                    name: tag,
                  },
                },
              },
            })),
          },
        },
      });
      return cocktail ? true : false;
    },
    deleteCocktail: async (_, { id }, { session }) => {
      if (!hasAccess(session, true)) return false;

      await prisma.cocktail.delete({ where: { id } });
      return true;
    },
    editCocktail: async (_, data, { session }) => {
      if (!hasAccess(session, true)) return false;

      await prisma.cocktail.delete({
        where: { id: data.id },
      });

      const cocktail = await prisma.cocktail.create({
        data: {
          ...data,
          ingredients: {
            create: data.ingredients.map((ingredient) => ({
              ingredient: {
                connectOrCreate: {
                  where: {
                    name: ingredient,
                  },
                  create: {
                    name: ingredient,
                    isAvailable: false,
                  },
                },
              },
            })),
          },
          tags: {
            create: data.tags.map((tag) => ({
              tag: {
                connectOrCreate: {
                  where: {
                    name: tag,
                  },
                  create: {
                    name: tag,
                  },
                },
              },
            })),
          },
        },
      });
      return cocktail ? true : false;
    },
    updateIngredientAvailability: async (_, { name, status }, { session }) => {
      if (!hasAccess(session, true)) return false;

      const res = await prisma.ingredient.update({
        where: { name },
        data: {
          isAvailable: status,
        },
      });
      return res ? true : false;
    },
    addSpirit: async (_, data, { session }) => {
      if (!hasAccess(session, true)) return false;

      const spirit = await prisma.spirit.create({
        data: {
          ...data,
          tags: {
            create: data.tags.map((tag) => ({
              tag: {
                connectOrCreate: {
                  where: {
                    name: tag,
                  },
                  create: {
                    name: tag,
                  },
                },
              },
            })),
          },
        },
      });
      return spirit ? true : false;
    },
    deleteSpirit: async (_, { id }, { session }) => {
      if (!hasAccess(session, true)) return false;

      await prisma.spirit.delete({ where: { id } });
      return true;
    },
    editSpirit: async (_, data, { session }) => {
      if (!hasAccess(session, true)) return false;

      console.log(data);

      await prisma.spirit.delete({
        where: { id: data.id },
      });

      const spirit = await prisma.spirit.create({
        data: {
          ...data,
          tags: {
            create: data.tags.map((tag) => ({
              tag: {
                connectOrCreate: {
                  where: {
                    name: tag,
                  },
                  create: {
                    name: tag,
                  },
                },
              },
            })),
          },
        },
      });
      return spirit ? true : false;
    },
    updateSpiritAvailability: async (_, { id, status }, { session }) => {
      if (!hasAccess(session, true)) return false;

      const res = await prisma.spirit.update({
        where: { id },
        data: {
          isAvailable: status,
        },
      });
      return res ? true : false;
    },
    updateBeerArchive: async (_, { id, status }, { session }) => {
      if (!hasAccess(session, true)) return false;

      const res = await prisma.beer.update({
        where: { id },
        data: {
          isArchived: status,
        },
      });
      return res ? true : false;
    },
    updateCocktailArchive: async (_, { id, status }, { session }) => {
      if (!hasAccess(session, true)) return false;

      const res = await prisma.cocktail.update({
        where: { id },
        data: {
          isArchived: status,
        },
      });
      return res ? true : false;
    },
    updateSpiritArchive: async (_, { id, status }, { session }) => {
      if (!hasAccess(session, true)) return false;

      const res = await prisma.spirit.update({
        where: { id },
        data: {
          isArchived: status,
        },
      });
      return res ? true : false;
    },
    addPost: async (_, { title, content }, { session }) => {
      if (!hasAccess(session, true)) return false;

      const res = await prisma.post.create({
        data: {
          submitter: session.user.name || "PapperlaPub",
          title,
          content,
        },
      });
      return res ? true : false;
    },
    sendPost: async (_, { title, content, receivers }, { session }) => {
      if (!hasAccess(session, true)) return false;

      const transporter = nodemailer.createTransport({
        host: process.env.MAILER_HOST,
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
          user: process.env.MAILER_USERNAME,
          pass: process.env.MAILER_PASSWORD,
        },
      });

      if (!transporter) return false;

      receivers
        .filter((r) => r.replace(" ", "") != "")
        .map(async (r) => {
          await transporter.sendMail({
            from: process.env.MAILER_NAME,
            to: r.replace(" ", ""),
            replyTo: config.mailReplyTo,
            subject: title,
            html: content,
          });
        });

      return true;
    },
    deletePost: async (_, { id }, { session }) => {
      if (!hasAccess(session, true)) return false;

      await prisma.post.delete({
        where: { id },
      });

      return true;
    },
    import: async (_, { excelLine }, { session }) => {
      if (!hasAccess(session, true)) return false;

      const parts = excelLine.split("\t");
      const beer = {
        name: parts[0].split(" ")[1],
        brewery: parts[0].split(" ")[0],
        menuOverride: parts[0],
        type: parts[2],
        price: Number(parts[7].split("Fr.")[1]),
        volume: parseInt(parts[5].slice(-2)),
        category: "seasonal",
        isAvailable: false,
        isArchived: false,
        alcohol:
          !isNaN(parts[11]) && !isNaN(parseFloat(parts[11]))
            ? parseFloat(parts[11])
            : 0,
        onTap: parts[5][0] == "G",
      };

      const addedBeer = await prisma.beer.create({
        data: { ...beer },
      });
      return addedBeer ? true : false;
    },
    addComment: async (_, { comment, beerId }, { session }) => {
      if (!hasAccess(session, false)) return false;

      const cmt = await prisma.comment.create({
        data: {
          comment,
          beer: {
            connect: {
              id: beerId,
            },
          },
          submitter: {
            connectOrCreate: {
              where: {
                sub: session.info.payload.sub,
              },
              create: {
                sub: session.info.payload.sub,
                name: session.user.name,
              },
            },
          },
        },
      });

      return cmt ? true : false;
    },
    deleteComment: async (_, { id }, { session }) => {
      if (!hasAccess(session, true)) return false;

      const cmt = await prisma.comment.delete({
        where: {
          id,
        },
      });

      return true;
    },
    addOrUpdateRating: async (_, { beerId, rating }, { session }) => {
      if (!hasAccess(session, false)) return false;

      const sub = session.info.payload.sub;

      const rt = await prisma.rating.findMany({
        where: {
          beer: {
            id: beerId,
          },
          user: {
            sub,
          },
        },
      });

      if (rt.length > 0) {
        const r = await prisma.rating.update({
          where: {
            id: rt[0].id,
          },
          data: {
            rating,
          },
        });
      } else {
        const r = await prisma.rating.create({
          data: {
            rating,
            beer: {
              connect: {
                id: beerId,
              },
            },
            user: {
              connectOrCreate: {
                where: {
                  sub,
                },
                create: {
                  sub,
                  name: session.user.name,
                },
              },
            },
          },
        });
      }

      return true;
    },
    addOrUpdateFlavour: async (
      _,
      { beerId, sweet, sour, bitter, malty, hoppy, fruity },
      { session }
    ) => {
      if (!hasAccess(session, false)) return false;

      const sub = session.info.payload.sub;

      const fl = await prisma.flavour.findMany({
        where: {
          beer: {
            id: beerId,
          },
          user: {
            sub,
          },
        },
      });

      if (fl.length > 0) {
        const r = await prisma.flavour.update({
          where: {
            id: fl[0].id,
          },
          data: {
            sweet,
            sour,
            bitter,
            malty,
            hoppy,
            fruity,
          },
        });
      } else {
        const f = await prisma.flavour.create({
          data: {
            sweet,
            sour,
            bitter,
            malty,
            hoppy,
            fruity,
            beer: {
              connect: {
                id: beerId,
              },
            },
            user: {
              connectOrCreate: {
                where: {
                  sub,
                },
                create: {
                  sub,
                  name: session.user.name,
                },
              },
            },
          },
        });
      }

      return true;
    },
  },
};
