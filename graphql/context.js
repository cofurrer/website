// @ts-ignore
import { PrismaClient } from "@prisma/client";
import prisma from "../lib/prisma";

import { NextApiRequest, NextApiResponse } from "next";

export async function createContext(req, res) {
  return {
    prisma,
  };
}
