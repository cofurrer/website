import { useState, useEffect } from "react";

import {
  ActionIcon,
  Autocomplete,
  Badge,
  Button,
  Checkbox,
  Grid,
  Group,
  Modal,
  NumberInput,
  Select,
  TextInput,
  useMantineTheme,
} from "@mantine/core";
import { useForm } from "@mantine/form";

import { Icon, ICONS } from "vseth-canine-ui";

import { useTranslation } from "next-i18next";

import { getIconColor } from "../utilities/colors";

import config from "../papperlaweb.config";

import { gql, useQuery, useMutation } from "@apollo/client";

const addSpiritMutation = gql`
  mutation addSpirit(
    $name: String
    $type: String
    $price: Float
    $alcohol: Float
    $tags: [String]
    $isAvailable: Boolean
    $isArchived: Boolean
  ) {
    addSpirit(
      name: $name
      type: $type
      price: $price
      alcohol: $alcohol
      tags: $tags
      isAvailable: $isAvailable
      isArchived: $isArchived
    )
  }
`;

const editSpiritMutation = gql`
  mutation editSpirit(
    $id: Int
    $name: String
    $type: String
    $price: Float
    $alcohol: Float
    $tags: [String]
    $isAvailable: Boolean
    $isArchived: Boolean
  ) {
    editSpirit(
      id: $id
      name: $name
      type: $type
      price: $price
      alcohol: $alcohol
      tags: $tags
      isAvailable: $isAvailable
      isArchived: $isArchived
    )
  }
`;

const getTagsQuery = gql`
  {
    getTags {
      name
    }
  }
`;

export default function AddSpiritModal({ open, close, spirit, refetch }) {
  const [addSpirit] = useMutation(addSpiritMutation);
  const [editSpirit] = useMutation(editSpiritMutation);
  const { data: tags, refetch: refetchTags } = useQuery(getTagsQuery);

  const [tag, setTag] = useState("");

  const { t } = useTranslation("common");

  const theme = useMantineTheme();

  const initialValues = {
    name: "",
    type: "",
    price: null,
    alcohol: null,
    tags: [],
    isAvailable: false,
    isArchived: false,
  };

  const form = useForm({
    initialValues: initialValues,

    validate: {
      name: (value) => (value ? null : t("ENotEmpty")),
      type: (value) => (value ? null : t("ENotEmpty")),
      price: (value) => (value ? null : t("ENotEmpty")),
      alcohol: (value) => (value ? null : t("ENotEmpty")),
    },
  });

  const setSpirit = () => {
    if (spirit) {
      form.setValues({
        ...spirit,
        tags: spirit.tags.map((tag) => {
          return tag.tag.name;
        }),
      });
    } else {
      form.setValues(initialValues);
    }
  };

  useEffect(() => {
    setSpirit();
  }, [spirit]);

  const submit = async (values) => {
    if (spirit) {
      const res = await editSpirit({
        variables: {
          id: spirit.id,
          ...values,
        },
      });
      if (res.data.editSpirit) {
        refetch();
        close();
      }
    } else {
      const res = await addSpirit({
        variables: {
          ...values,
        },
      });
      if (res.data.addSpirit) {
        refetch();
        close();
      }
    }
  };

  const formatTags = () => {
    if (!tags?.getTags) return [];
    return tags.getTags.map((tag) => {
      return tag.name;
    });
  };

  const addTag = () => {
    const current = form.values.tags;
    if (!current.includes(tag)) current.push(tag);
    form.setValues({
      tags: current,
    });
    setTag("");
  };

  const removeTag = (tag) => {
    const current = form.values.tags;
    const newArr = current.filter((t) => t != tag);
    form.setValues({ tags: newArr });
  };

  return (
    <Modal opened={open} onClose={close} size="xl">
      <div style={{ textAlign: "center" }}>
        <h2>{spirit ? t("editSpirit") : t("addSpirit")}</h2>
      </div>

      <form onSubmit={form.onSubmit((values) => submit(values))}>
        <Grid>
          <Grid.Col sm={12} md={6}>
            <TextInput
              withAsterisk
              label={t("name")}
              placeholder="Ardbeg Corryvreckan 12 years"
              {...form.getInputProps("name")}
            />
          </Grid.Col>
          <Grid.Col sm={12} md={6}>
            <TextInput
              withAsterisk
              label={t("type")}
              placeholder="Single Malt Whiskey"
              {...form.getInputProps("type")}
            />
          </Grid.Col>
          <Grid.Col sm={12} md={6}>
            <NumberInput
              withAsterisk
              label={t("price")}
              placeholder="7"
              min={0}
              icon="CHF"
              precision={2}
              {...form.getInputProps("price")}
            />
          </Grid.Col>
          <Grid.Col sm={12} md={6}>
            <NumberInput
              withAsterisk
              label={t("alcohol")}
              placeholder="40"
              min={0}
              step={0.1}
              icon="%"
              precision={1}
              {...form.getInputProps("alcohol")}
            />
          </Grid.Col>
          <Grid.Col sm={12} md={6}>
            <Group align="flex-end">
              <Autocomplete
                withAsterisk
                value={tag}
                onChange={setTag}
                label={t("tags")}
                placeholder="Tiki"
                data={formatTags()}
                style={{ flex: 1 }}
              />
              <Button onClick={addTag}>{t("add")}</Button>
            </Group>
            <Group spacing={7} mt={7}>
              {form.values.tags.map((tag, i) => (
                <Badge
                  key={i}
                  rightSection={
                    <ActionIcon size="xs" onClick={() => removeTag(tag)}>
                      <Icon
                        size={10}
                        icon={ICONS.CLOSE}
                        color={getIconColor(theme)}
                      />
                    </ActionIcon>
                  }
                >
                  {tag}
                </Badge>
              ))}
            </Group>
          </Grid.Col>
          <Grid.Col sm={12} md={3}>
            <Checkbox
              mt="xl"
              label={t("currentlyAvailable")}
              {...form.getInputProps("isAvailable", { type: "checkbox" })}
            />
          </Grid.Col>
          <Grid.Col sm={12}>
            <Button type="submit">{t("submit")}</Button>
          </Grid.Col>
        </Grid>
      </form>
    </Modal>
  );
}
