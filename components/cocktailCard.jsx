import { ReactNode } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import Image from "next/image";
import { useTranslation } from "next-i18next";

import hasAccess from "../utilities/hasAccess";
import isAvailable from "../utilities/isAvailable";
import {
  getTextColor,
  getAccentColor,
  getDisabledColor,
  getIconColor,
} from "../utilities/colors";

import { useSession } from "next-auth/react";
import { useInterval } from "@mantine/hooks";
import {
  ActionIcon,
  Alert,
  Badge,
  Blockquote,
  Button,
  Card,
  createStyles,
  Divider,
  Group,
  List,
  Progress,
  Rating,
  Skeleton,
  Space,
  Switch,
  Text,
  TextInput,
  TextInputProps,
  Tooltip,
  useMantineTheme,
} from "@mantine/core";

import { Icon, ICONS } from "vseth-canine-ui";

import { gql, useMutation } from "@apollo/client";

const updateArchiveMutation = gql`
  mutation updateCocktailArchive($id: Int, $status: Boolean) {
    updateCocktailArchive(id: $id, status: $status)
  }
`;

const updateIngredientAvailabilityMutation = gql`
  mutation updateIngredientAvailability($name: String, $status: Boolean) {
    updateIngredientAvailability(name: $name, status: $status)
  }
`;

export default function CocktailCard({
  cocktail,
  editCocktail,
  refetch,
  isArchive,
  showIngredientsList = false,
}) {
  const [updateCocktailArchive] = useMutation(updateArchiveMutation);
  const [updateIngredientAvailability] = useMutation(
    updateIngredientAvailabilityMutation
  );

  const { locale } = useRouter();
  const { data: session } = useSession();
  const theme = useMantineTheme();
  const { t } = useTranslation("common");

  const toggleArchive = async () => {
    const res = await updateCocktailArchive({
      variables: {
        id: cocktail.id,
        status: !cocktail.isArchived,
      },
    });
    if (res) refetch();
  };

  const isAvail = isAvailable(cocktail);

  const textColor = isAvail ? getTextColor(theme) : getDisabledColor();
  const accentColor = isAvail ? getAccentColor(theme) : getDisabledColor();

  const updateAvailability = async (name, status) => {
    const res = await updateIngredientAvailability({
      variables: {
        name,
        status: !status,
      },
    });
    if (res) {
      refetch();
    }
  };

  return (
    <Card
      shadow="md"
      radius="md"
      style={{
        // border: isAvail && "1px solid #4f2a17",
        display: "flex",
        justifyContent: "space-between",
        flexDirection: "column",
        width: "100%",
        color: textColor,
      }}
      withBorder
    >
      <div>
        {/*cocktail.imageLandscape && (
          <Card.Section
            style={{
              position: "relative",
              aspectRatio: "16/9",
              overflow: "hidden",
              marginBottom: "1rem",
              marginTop: "-1rem",
            }}
          >
            <Image
              src={cocktail.imageLandscape}
              width={0}
              height={0}
              sizes="100vw"
              style={{ width: "100%", height: "auto" }}
              alt="Cocktail Photo"
            />
            <Space h="md" />
          </Card.Section>
        )*/}

        <div>
          <Group>
            <h2 style={{ margin: 0 }}>{cocktail.name}</h2>
            {!cocktail.containsAlcohol && (
              <Badge color="green">{t("alcoholFree")}</Badge>
            )}
          </Group>
          <List spacing="xs" size="md" center style={{ color: textColor }}>
            <Group spacing={7} mt={7}>
              {cocktail.ingredients.map((ingredient, i) => (
                <Badge key={i} color="orange">
                  {ingredient.ingredient.name}
                </Badge>
              ))}
            </Group>
            <Group spacing={7} mt={7}>
              {cocktail.tags.map((tag, i) => (
                <Badge key={i}>{tag.tag.name}</Badge>
              ))}
            </Group>
            <Space h="xl" />
            <List.Item
              icon={<Icon icon={ICONS.MONEY} color={accentColor} size={22} />}
            >
              <p>{cocktail.price} CHF</p>
            </List.Item>
          </List>
        </div>

        {showIngredientsList && (
          <>
            <Divider my="sm" />
            <List spacing="xs" size="md" center style={{ color: textColor }}>
              {cocktail.ingredients.map((ingredient) => (
                <List.Item
                  icon={
                    <Switch
                      checked={ingredient.ingredient.isAvailable}
                      onChange={() =>
                        updateAvailability(
                          ingredient.ingredient.name,
                          ingredient.ingredient.isAvailable
                        )
                      }
                    />
                  }
                  key={ingredient.ingredient.name}
                >
                  <p>{ingredient.ingredient.name}</p>
                </List.Item>
              ))}
            </List>
            <Divider my="sm" />
          </>
        )}
      </div>

      <Space h="md" />
      <div>
        {hasAccess(session, true) && (
          <Group style={{ verticalAlign: "middle" }}>
            {!isArchive && (
              <Button
                leftIcon={
                  <Icon icon={ICONS.EDIT} color={getIconColor(theme)} />
                }
                variant="light"
                onClick={() => editCocktail(cocktail)}
              >
                {t("editCocktail")}
              </Button>
            )}
            {!isArchive && <div style={{ flex: 1 }}></div>}
            <Tooltip
              label={isArchive ? t("moveOutOfGraveyard") : t("moveToGraveyard")}
              withArrow
            >
              <ActionIcon variant="subtle" onClick={toggleArchive}>
                <Icon
                  icon={isArchive ? ICONS.INBOX_UP : ICONS.INBOX_DOWN}
                  color={getIconColor(theme)}
                />
              </ActionIcon>
            </Tooltip>
          </Group>
        )}
      </div>
    </Card>
  );
}
