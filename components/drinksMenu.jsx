import { useState } from "react";

import { Button, Grid, Space, TextInput, useMantineTheme } from "@mantine/core";

import { useDebouncedState } from "@mantine/hooks";

import BeerCard from "../components/beerCard";
import AddBeerModal from "../components/addBeerModal";

import hasAccess from "../utilities/hasAccess";
import { getIconColor } from "../utilities/colors";

import { useTranslation } from "next-i18next";
import { useSession } from "next-auth/react";

import { Icon, ICONS } from "vseth-canine-ui";

import { gql, useQuery, useMutation } from "@apollo/client";

const updateAvailabilityMutation = gql`
  mutation updateAvailability($id: Int, $status: Boolean) {
    updateAvailability(id: $id, status: $status)
  }
`;

export default function DrinksMenu({
  beers,
  refetch,
  isArchive = false,
  showButtons = false,
}) {
  const [updateAvailability] = useMutation(updateAvailabilityMutation);

  const [selection, setSelection] = useState(null);
  const [modalOpen, setModalOpen] = useState(false);
  const [filter, setFilter] = useDebouncedState("", 200);

  const { data: session } = useSession();
  const { t } = useTranslation("common");

  const theme = useMantineTheme();

  const edit = (beer) => {
    setSelection(beer);
    setModalOpen(true);
  };

  const add = () => {
    setSelection(null);
    setModalOpen(true);
  };

  const updateAvail = async (id, status) => {
    const res = await updateAvailability({
      variables: {
        id,
        status: !status,
      },
    });
    if (res.data.updateAvailability) refetch();
  };

  const matchesFilter = (beer) => {
    const combinedString =
      beer.name.toLowerCase() +
      " " +
      beer.brewery.toLowerCase() +
      " " +
      beer.type.toLowerCase();
    return combinedString.includes(filter.toLowerCase());
  };

  return (
    <>
      <div style={{ textAlign: "center" }}>
        <h1>{t("beers")}</h1>
        {hasAccess(session, true) && !isArchive && (
          <>
            <Button
              leftIcon={<Icon icon={ICONS.PLUS} color="white" />}
              onClick={add}
            >
              {t("addBeer")}
            </Button>
            <Space h="md" />
          </>
        )}
      </div>

      <TextInput
        defaultValue={filter}
        onChange={(e) => setFilter(e.target.value)}
        icon={<Icon icon={ICONS.SEARCH} color={getIconColor(theme)} />}
        placeholder={t("search")}
        size="md"
      />

      <Space h="md" />
      <Grid>
        {beers &&
          beers
            .filter(
              (beer) =>
                (beer.isAvailable || hasAccess(session, true)) &&
                matchesFilter(beer)
            )
            .map((beer) => (
              <Grid.Col
                xs={12}
                sm={6}
                md={4}
                key={beer.id}
                style={{ display: "flex" }}
              >
                <BeerCard
                  beer={beer}
                  editBeer={edit}
                  updateAvailability={updateAvail}
                  refetch={refetch}
                  isArchive={isArchive}
                  showButtons={showButtons}
                />
              </Grid.Col>
            ))}
      </Grid>

      <AddBeerModal
        open={modalOpen}
        close={() => setModalOpen(false)}
        beer={selection}
        refetch={refetch}
      />
    </>
  );
}
