import { Space, useMantineTheme } from "@mantine/core";

import { useTranslation } from "next-i18next";

import { formatDate } from "../utilities/dataFormatter";
import { getAccentColor } from "../utilities/colors";

import parse from "html-react-parser";

export default function BlogPost({ post }) {
  const { t } = useTranslation("common");
  const theme = useMantineTheme();

  return (
    <div>
      <h2 style={{ margin: 0 }}>{post.title}</h2>
      <p style={{ margin: 0 }}>
        {formatDate(post.date)} {t("by")}
        <span style={{ color: "#824e34" }}>{post.submitter}</span>
      </p>
      <Space h="md" />
      <div className="blogPost">{parse(post.content)}</div>
      <Space h="xl" />
    </div>
  );
}
