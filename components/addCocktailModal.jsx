import { useState, useEffect } from "react";

import {
  ActionIcon,
  Autocomplete,
  Badge,
  Button,
  Checkbox,
  FileInput,
  Grid,
  Group,
  Modal,
  NumberInput,
  TextInput,
  useMantineTheme,
} from "@mantine/core";
import { useForm } from "@mantine/form";

import storeImage from "../utilities/storeImage";
import { getIconColor } from "../utilities/colors";

import { useTranslation } from "next-i18next";

import { Icon, ICONS } from "vseth-canine-ui";

import { gql, useQuery, useMutation } from "@apollo/client";

const addCocktailMutation = gql`
  mutation addCocktail(
    $name: String
    $price: Float
    $ingredients: [String]
    $tags: [String]
    $containsAlcohol: Boolean
    $imagePortrait: String
    $imageLandscape: String
  ) {
    addCocktail(
      name: $name
      price: $price
      ingredients: $ingredients
      tags: $tags
      containsAlcohol: $containsAlcohol
      imagePortrait: $imagePortrait
      imageLandscape: $imageLandscape
    )
  }
`;

const editCocktailMutation = gql`
  mutation editCocktail(
    $id: Int
    $name: String
    $price: Float
    $ingredients: [String]
    $tags: [String]
    $containsAlcohol: Boolean
    $imagePortrait: String
    $imageLandscape: String
  ) {
    editCocktail(
      id: $id
      name: $name
      price: $price
      ingredients: $ingredients
      tags: $tags
      containsAlcohol: $containsAlcohol
      imagePortrait: $imagePortrait
      imageLandscape: $imageLandscape
    )
  }
`;

const getIngredientsQuery = gql`
  {
    getIngredients {
      name
    }
  }
`;

const getTagsQuery = gql`
  {
    getTags {
      name
    }
  }
`;

export default function AddCocktailModal({ open, close, cocktail, refetch }) {
  const [addCocktail] = useMutation(addCocktailMutation);
  const [editCocktail] = useMutation(editCocktailMutation);
  const { data: ingredients, refetch: refetchIngredients } =
    useQuery(getIngredientsQuery);
  const { data: tags, refetch: refetchTags } = useQuery(getTagsQuery);

  const [ingredient, setIngredient] = useState("");
  const [tag, setTag] = useState("");
  const [loading, setLoading] = useState(false);

  const { t } = useTranslation("common");

  const theme = useMantineTheme();

  const initialValues = {
    name: "",
    price: null,
    ingredients: [],
    tags: [],
    containsAlcohol: true,
    imagePortrait: null,
    imageLandscape: null,
  };

  const form = useForm({
    initialValues: initialValues,

    validate: {
      name: (value) => (value ? null : t("ENotEmpty")),
      price: (value) => (value ? null : t("ENotEmpty")),
      ingredients: (value) => (value.length > 0 ? null : t("ENotEmpty")),
      //imagePortrait: (value, values) => ((!value && values.imageLandscape) ? t("ENotEmpty") : null),
      //imageLandscape: (value, values) => ((!value && values.imagePortrait) ? t("ENotEmpty") : null),
    },
  });

  const setCocktail = () => {
    setLoading(false);
    if (cocktail) {
      form.setValues({
        ...cocktail,
        ingredients: cocktail.ingredients.map((ing) => {
          return ing.ingredient.name;
        }),
        tags: cocktail.tags.map((tag) => {
          return tag.tag.name;
        }),
      });
    } else {
      form.setValues(initialValues);
    }
  };

  useEffect(() => {
    setCocktail();
  }, [cocktail]);

  const formatIngredients = () => {
    if (!ingredients?.getIngredients) return [];
    return ingredients.getIngredients.map((ingredient) => {
      return ingredient.name;
    });
  };

  const addIngredient = () => {
    const current = form.values.ingredients;
    if (!current.includes(ingredient)) current.push(ingredient);
    form.setValues({
      ingredients: current,
    });
    setIngredient("");
  };

  const removeIngredient = (ingredient) => {
    const current = form.values.ingredients;
    const newArr = current.filter((ing) => ing != ingredient);
    form.setValues({ ingredients: newArr });
  };

  const formatTags = () => {
    if (!tags?.getTags) return [];
    return tags.getTags.map((tag) => {
      return tag.name;
    });
  };

  const addTag = () => {
    const current = form.values.tags;
    if (!current.includes(tag)) current.push(tag);
    form.setValues({
      tags: current,
    });
    setTag("");
  };

  const removeTag = (tag) => {
    const current = form.values.tags;
    const newArr = current.filter((t) => t != tag);
    form.setValues({ tags: newArr });
  };

  const removeImage = () => {
    form.setValues({
      imageLandscape: null,
    });
  };

  const submit = async (values) => {
    setLoading(true);
    if (cocktail) {
      const portrait = await storeImage(values.imagePortrait);
      const landscape = await storeImage(values.imageLandscape);

      const res = await editCocktail({
        variables: {
          id: cocktail.id,
          ...values,
          imagePortrait: portrait,
          imageLandscape: landscape,
        },
      });
      if (res.data.editCocktail) {
        refetch();
        refetchIngredients();
        refetchTags();
        form.reset();
        close();
      }
    } else {
      const portrait = await storeImage(values.imagePortrait);
      const landscape = await storeImage(values.imageLandscape);

      const res = await addCocktail({
        variables: {
          ...values,
          imagePortrait: portrait,
          imageLandscape: landscape,
        },
      });
      if (res.data.addCocktail) {
        refetch();
        refetchIngredients();
        refetchTags();
        form.reset();
        close();
      }
    }
  };

  return (
    <Modal opened={open} onClose={close} size="xl">
      <div style={{ textAlign: "center" }}>
        <h2>{cocktail ? t("editCocktail") : t("addCocktail")}</h2>
      </div>

      <form onSubmit={form.onSubmit((values) => submit(values))}>
        <Grid>
          <Grid.Col sm={12} md={6}>
            <TextInput
              withAsterisk
              label={t("name")}
              placeholder="Mojito"
              {...form.getInputProps("name")}
            />
          </Grid.Col>
          <Grid.Col sm={12} md={6}>
            <NumberInput
              withAsterisk
              label={t("price")}
              placeholder="7"
              min={0}
              icon="CHF"
              {...form.getInputProps("price")}
            />
          </Grid.Col>
          <Grid.Col sm={12} md={6}>
            <Group align="flex-end">
              <Autocomplete
                withAsterisk
                value={ingredient}
                onChange={setIngredient}
                label={t("ingredients")}
                placeholder="London Dry Gin"
                data={formatIngredients()}
                style={{ flex: 1 }}
              />
              <Button onClick={addIngredient}>{t("add")}</Button>
            </Group>
            <Group spacing={7} mt={7}>
              {form.values.ingredients.map((ingredient, i) => (
                <Badge
                  key={i}
                  rightSection={
                    <ActionIcon
                      size="xs"
                      onClick={() => removeIngredient(ingredient)}
                    >
                      <Icon
                        size={10}
                        icon={ICONS.CLOSE}
                        color={getIconColor(theme)}
                      />
                    </ActionIcon>
                  }
                  color="green"
                >
                  {ingredient}
                </Badge>
              ))}
            </Group>
          </Grid.Col>
          <Grid.Col sm={12} md={6}>
            <Group align="flex-end">
              <Autocomplete
                withAsterisk
                value={tag}
                onChange={setTag}
                label={t("tags")}
                placeholder="Tiki"
                data={formatTags()}
                style={{ flex: 1 }}
              />
              <Button onClick={addTag}>{t("add")}</Button>
            </Group>
            <Group spacing={7} mt={7}>
              {form.values.tags.map((tag, i) => (
                <Badge
                  key={i}
                  rightSection={
                    <ActionIcon size="xs" onClick={() => removeTag(tag)}>
                      <Icon
                        size={10}
                        icon={ICONS.CLOSE}
                        color={getIconColor(theme)}
                      />
                    </ActionIcon>
                  }
                >
                  {tag}
                </Badge>
              ))}
            </Group>
          </Grid.Col>
          {/*<Grid.Col sm={12} md={6}>
            <FileInput
              {...form.getInputProps("imagePortrait")}
              icon={<Icon icon={ICONS.IMAGE} color="#4f2a17" />}
              label={t('imagePortrait')}
              placeholder={t('clickHere')}
              accept="image/png,image/jpeg"
            />
          </Grid.Col>*/}
          <Grid.Col sm={12}>
            <Group align="flex-end">
              <FileInput
                {...form.getInputProps("imageLandscape")}
                icon={<Icon icon={ICONS.IMAGE} color={getIconColor(theme)} />}
                label={t("imageLandscape")}
                placeholder={t("clickHere")}
                accept="image/png,image/jpeg"
                description={
                  form.values.imageLandscape ? "File already uploaded" : ""
                }
                style={{ flex: 1 }}
              />
              <Button onClick={removeImage}>{t("removeImage")}</Button>
            </Group>
          </Grid.Col>
          <Grid.Col sm={12}>
            <Checkbox
              mt="xl"
              label={t("containsAlcohol")}
              {...form.getInputProps("containsAlcohol", { type: "checkbox" })}
            />
          </Grid.Col>
          <Grid.Col xs={12}>
            <Button type="submit" loading={loading}>
              {t("submit")}
            </Button>
          </Grid.Col>
        </Grid>
      </form>
    </Modal>
  );
}
