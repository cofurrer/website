import { useState } from "react";

import {
  Accordion,
  Grid,
  Group,
  Space,
  Switch,
  TextInput,
  useMantineTheme,
} from "@mantine/core";
import { useDebouncedState } from "@mantine/hooks";

import { Icon, ICONS } from "vseth-canine-ui";

import { useTranslation } from "next-i18next";

import { gql, useQuery, useMutation } from "@apollo/client";

import { getIconColor } from "../utilities/colors";

const getIngredientsQuery = gql`
  {
    getIngredients {
      name
      isAvailable
    }
  }
`;

const updateIngredientAvailabilityMutation = gql`
  mutation updateIngredientAvailability($name: String, $status: Boolean) {
    updateIngredientAvailability(name: $name, status: $status)
  }
`;

export default function IngredientsList({ refetch }) {
  const { data: ingredients, refetch: refetchI } =
    useQuery(getIngredientsQuery);
  const [updateIngredientAvailability] = useMutation(
    updateIngredientAvailabilityMutation
  );

  const [filter, setFilter] = useDebouncedState("", 200);

  const { t } = useTranslation("common");

  const theme = useMantineTheme();

  const updateAvailability = async (name, status) => {
    const res = await updateIngredientAvailability({
      variables: {
        name,
        status: !status,
      },
    });
    if (res) {
      refetchI();
      refetch();
    }
  };

  const matchesFilter = (ingredient) => {
    return ingredient.toLowerCase().includes(filter.toLowerCase());
  };

  return (
    <Accordion style={{ marginTop: "20px" }}>
      <Accordion.Item value="blubb">
        <Accordion.Control>
          <div style={{ textAlign: "center" }}>
            <h2 style={{ margin: 0 }}>{t("ingredients")}</h2>
          </div>
        </Accordion.Control>
        <Accordion.Panel>
          <TextInput
            defaultValue={filter}
            onChange={(e) => setFilter(e.target.value)}
            icon={<Icon icon={ICONS.SEARCH} color={getIconColor(theme)} />}
            placeholder={t("search")}
            size="md"
          />

          <Space h="md" />
          {ingredients && (
            <Grid>
              {ingredients.getIngredients
                .filter((ingredient) => matchesFilter(ingredient.name))
                .map((ingredient) => (
                  <Grid.Col sx={12} sm={4} md={3} key={ingredient.name}>
                    <Group>
                      <Switch
                        checked={ingredient.isAvailable}
                        onChange={() =>
                          updateAvailability(
                            ingredient.name,
                            ingredient.isAvailable
                          )
                        }
                      />
                      <h3>{ingredient.name}</h3>
                    </Group>
                  </Grid.Col>
                ))}
            </Grid>
          )}
        </Accordion.Panel>
      </Accordion.Item>
    </Accordion>
  );
}
