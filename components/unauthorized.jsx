import { Container } from "@mantine/core";

import { useTranslation } from "next-i18next";

import parse from "html-react-parser";

export default function Unauthorized() {
  const { t } = useTranslation("common");
  return (
    <Container size="xl">
      <h1 style={{ margin: 0 }}>{t("error")}</h1>
      <h2 style={{ marginTop: 0 }}>401: Unauthorized</h2>

      <p>{parse(t("401"))}</p>
    </Container>
  );
}
