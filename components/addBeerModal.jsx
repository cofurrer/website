import { useEffect } from "react";

import {
  Button,
  Checkbox,
  Grid,
  Modal,
  NumberInput,
  Select,
  TextInput,
} from "@mantine/core";
import { useForm } from "@mantine/form";

import { useTranslation } from "next-i18next";

import config from "../papperlaweb.config";

import { gql, useMutation } from "@apollo/client";

const addBeerMutation = gql`
  mutation addBeer(
    $name: String
    $brewery: String
    $menuOverride: String
    $type: String
    $price: Float
    $volume: Float
    $category: String
    $isAvailable: Boolean
    $alcohol: Float
    $onTap: Boolean
  ) {
    addBeer(
      name: $name
      brewery: $brewery
      menuOverride: $menuOverride
      type: $type
      price: $price
      volume: $volume
      category: $category
      isAvailable: $isAvailable
      alcohol: $alcohol
      onTap: $onTap
    )
  }
`;

const editBeerMutation = gql`
  mutation editBeer(
    $id: Int
    $name: String
    $brewery: String
    $menuOverride: String
    $type: String
    $price: Float
    $volume: Float
    $category: String
    $isAvailable: Boolean
    $alcohol: Float
    $onTap: Boolean
  ) {
    editBeer(
      id: $id
      name: $name
      brewery: $brewery
      menuOverride: $menuOverride
      type: $type
      price: $price
      volume: $volume
      category: $category
      isAvailable: $isAvailable
      alcohol: $alcohol
      onTap: $onTap
    )
  }
`;

export default function AddBeerModal({ open, close, beer, refetch }) {
  const [addBeer] = useMutation(addBeerMutation);
  const [editBeer] = useMutation(editBeerMutation);

  console.log(open, close, beer, refetch);

  const { t } = useTranslation("common");

  const initialValues = {
    name: "",
    brewery: "",
    menuOverride: "",
    type: "",
    price: 0,
    volume: 0,
    category: "",
    isAvailable: false,
    alcohol: 0,
    onTap: false,
  };

  const form = useForm({
    initialValues: initialValues,

    validate: {
      name: (value) => (value ? null : t("ENotEmpty")),
      brewery: (value) => (value ? null : t("ENotEmpty")),
      type: (value) => (value ? null : t("ENotEmpty")),
      price: (value) => (value ? null : t("ENotEmpty")),
      volume: (value) => (value ? null : t("ENotEmpty")),
      category: (value) => (value ? null : t("ENotEmpty")),
      alcohol: (value) => ((value === 0 || value) ? null : t("ENotEmpty")),
    },
  });

  const setBeer = () => {
    if (beer) {
      form.setValues({ ...beer });
    } else {
      form.setValues(initialValues);
    }
  };

  useEffect(() => {
    setBeer();
  }, [beer]);

  const categories = config.categories;

  const categories_select = categories.map((cat) => ({
    value: cat.name,
    label: cat.name.replace(/(^|\s)\S/g, function (t) {
      return t.toUpperCase();
    }),
  }));

  const submit = async (values) => {
    if (beer) {
      const res = await editBeer({
        variables: {
          id: beer.id,
          ...values,
        },
      });
      if (res.data.editBeer) {
        refetch();
        close();
      }
    } else {
      const res = await addBeer({
        variables: {
          ...values,
        },
      });
      if (res.data.addBeer) {
        refetch();
        close();
      }
    }
  };

  return (
    <Modal opened={open} onClose={close} size="xl">
      <div style={{ textAlign: "center" }}>
        <h2>{beer ? t("editBeer") : t("addBeer")}</h2>
      </div>

      <form onSubmit={form.onSubmit((values) => submit(values))}>
        <Grid>
          <Grid.Col sm={12} md={6}>
            <TextInput
              withAsterisk
              label={t("name")}
              placeholder="Envoutée"
              {...form.getInputProps("name")}
            />
          </Grid.Col>
          <Grid.Col sm={12} md={6}>
            <TextInput
              withAsterisk
              label={t("brewery")}
              placeholder="Brasserie les Trois Dames"
              {...form.getInputProps("brewery")}
            />
          </Grid.Col>
          <Grid.Col sm={12} md={6}>
            <TextInput
              label={t("menuOverride")}
              placeholder="Brasserie les Trois Dames: Lager"
              {...form.getInputProps("menuOverride")}
            />
          </Grid.Col>
          <Grid.Col sm={12} md={6}>
            <TextInput
              withAsterisk
              label={t("style")}
              placeholder="Sour"
              {...form.getInputProps("type")}
            />
          </Grid.Col>
          <Grid.Col sm={12} md={6}>
            <NumberInput
              withAsterisk
              label={t("price")}
              placeholder="3"
              min={0}
              icon="CHF"
              precision={2}
              {...form.getInputProps("price")}
            />
          </Grid.Col>
          <Grid.Col sm={12} md={6}>
            <NumberInput
              withAsterisk
              label={t("amount")}
              placeholder="33"
              min={0}
              icon="cL"
              {...form.getInputProps("volume")}
            />
          </Grid.Col>
          <Grid.Col sm={12} md={6}>
            <NumberInput
              withAsterisk
              label={t("alcohol")}
              placeholder="4.7"
              min={0}
              step={0.1}
              icon="%"
              precision={1}
              {...form.getInputProps("alcohol")}
            />
          </Grid.Col>
          <Grid.Col sm={12} md={6}>
            <Select
              label={t("category")}
              placeholder="Standard"
              data={categories_select}
              {...form.getInputProps("category")}
            />
          </Grid.Col>
          <Grid.Col sm={12} md={3}>
            <Checkbox
              mt="xl"
              label={t("currentlyAvailable")}
              {...form.getInputProps("isAvailable", { type: "checkbox" })}
            />
          </Grid.Col>
          <Grid.Col sm={12} md={3}>
            <Checkbox
              mt="xl"
              label={t("onTap")}
              {...form.getInputProps("onTap", { type: "checkbox" })}
            />
          </Grid.Col>
          <Grid.Col sm={12}>
            <Button type="submit">{t("submit")}</Button>
          </Grid.Col>
        </Grid>
      </form>
    </Modal>
  );
}
