import { useState, useEffect } from "react";
import { useRouter } from "next/router";

import { useTranslation } from "next-i18next";

import { useSession } from "next-auth/react";

import { SegmentedControl, useMantineTheme } from "@mantine/core";

import { Radar } from "react-chartjs-2";

import {
  Chart as ChartJS,
  RadialLinearScale,
  PointElement,
  LineElement,
  Filler,
  Tooltip,
  Legend,
} from "chart.js";

import { getAccentColor } from "../utilities/colors";

ChartJS.register(
  RadialLinearScale,
  PointElement,
  LineElement,
  Filler,
  Tooltip,
  Legend
);

export default function FlavourSpider({ flavours, setAccValue }) {
  const { locale } = useRouter();
  const { t } = useTranslation("common");
  const theme = useMantineTheme();

  const [hoppy, setHoppy] = useState(0);
  const [malty, setMalty] = useState(0);
  const [fruity, setFruity] = useState(0);
  const [bitter, setBitter] = useState(0);
  const [sweet, setSweet] = useState(0);
  const [sour, setSour] = useState(0);

  const [mode, setMode] = useState("average");

  const { data: session } = useSession();

  const options = {
    scale: {
      min: 0.5,
      max: 5,
    },
    plugins: {
      legend: {
        display: false,
      },
    },
    scales: {
      r: {
        ticks: {
          backdropColor: "rgba(255, 255, 255, 0)",
          color: getAccentColor(theme),
        },
        title: {
          color: getAccentColor(theme),
          display: true,
        },
        grid: {
          color: "rgba(180,180,180,0.2)",
        },
        angleLines: {
          color: "rgba(180,180,180,0.2)",
        },
        pointLabels: {
          color: getAccentColor(theme),
        },
      },
    },
  };

  const calculateMean = () => {
    if (mode == "average") {
      let n_hoppy = 0;
      let n_malty = 0;
      let n_fruity = 0;
      let n_bitter = 0;
      let n_sweet = 0;
      let n_sour = 0;
      flavours.forEach((flavour) => {
        n_hoppy += flavour.hoppy;
        n_malty += flavour.malty;
        n_fruity += flavour.fruity;
        n_bitter += flavour.bitter;
        n_sweet += flavour.sweet;
        n_sour += flavour.sour;
      });
      if (!flavours) {
        setHoppy(0);
        setMalty(0);
        setFruity(0);
        setBitter(0);
        setSweet(0);
        setSour(0);
      } else {
        const len = flavours.length;
        setHoppy(n_hoppy / len);
        setMalty(n_malty / len);
        setFruity(n_fruity / len);
        setBitter(n_bitter / len);
        setSweet(n_sweet / len);
        setSour(n_sour / len);
      }
    } else {
      let found;
      if (session) {
        found = flavours.filter(
          (flavour) => flavour.user.sub == session.info.payload.sub
        );
      }
      if (found && found.length > 0 && session) {
        setHoppy(found[0].hoppy);
        setMalty(found[0].malty);
        setFruity(found[0].fruity);
        setBitter(found[0].bitter);
        setSweet(found[0].sweet);
        setSour(found[0].sour);
      } else {
        setHoppy(0);
        setMalty(0);
        setFruity(0);
        setBitter(0);
        setSweet(0);
        setSour(0);
      }
    }
  };

  useEffect(() => {
    calculateMean();
  }, [flavours, mode]);

  const labels =
    locale === "en"
      ? ["hoppy", "malty", "fruity", "bitter", "sweet", "sour"]
      : ["hopfig", "malzig", "fruchtig", "bitter", "süss", "sauer"];

  return (
    <div
      style={{
        textAlign: "center",
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-between",
        width: "100%",
      }}
    >
      <div></div>
      <div>
        {hoppy > 0 ? (
          <Radar
            data={{
              labels: labels,
              datasets: [
                {
                  data: [hoppy, malty, fruity, bitter, sweet, sour],
                  backgroundColor: "rgba(210, 135, 18, 0.2)",
                  borderColor: "rgba(210, 135, 18, 1)",
                  borderWidth: 1,
                },
              ],
            }}
            options={options}
          />
        ) : (
          <p>
            {t("nothingHereFlavour")}{" "}
            <a href="#flavourProfile" onClick={() => setAccValue("flavour")}>
              {t("here")}
            </a>
            {t("hinzu")}
          </p>
        )}
      </div>
      <div>
        <SegmentedControl
          value={mode}
          onChange={setMode}
          data={[
            { label: t("average"), value: "average" },
            { label: t("yours"), value: "yours" },
          ]}
          mt="md"
        />
      </div>
    </div>
  );
}
