import { useRouter } from "next/router";

import {
  ActionIcon,
  Box,
  Card,
  Grid,
  MediaQuery,
  useMantineTheme,
} from "@mantine/core";

import { Icon, ICONS } from "vseth-canine-ui";

import { useTranslation } from "next-i18next";

import parse from "html-react-parser";

import BlogPost from "../components/blogPost";
import { getAccentColor } from "../utilities/colors";

import config from "../papperlaweb.config";

import { gql, useQuery } from "@apollo/client";

const getNewestPostQuery = gql`
  {
    getNewestPost {
      date
      submitter
      title
      content
    }
  }
`;

export default function MainHeader() {
  const { data: post } = useQuery(getNewestPostQuery);
  const { locale } = useRouter();
  const { t } = useTranslation("common");
  const theme = useMantineTheme();

  const stripeColor = theme.colorScheme == "dark" ? "#4f2a17" : "#f2d6c9";
  const stripeColor2 = theme.colorScheme == "dark" ? "#25262b" : "#ffffff";

  return (
    <div
      style={{
        height: "calc(100vh - 112px)",
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-evenly",
      }}
    >
      <Grid style={{ display: "flex", alignItems: "center" }}>
        <MediaQuery
          smallerThan="md"
          styles={{
            textAlign: "center",
            marginTop: "50px",
            marginBottom: "50px",
          }}
        >
          <Grid.Col md={6} xs={12}>
            <p>{t("welcomeTo")}</p>
            <h1
              style={{ margin: 0, fontFamily: "Futura", fontWeight: "normal" }}
            >
              PapperlaPub
            </h1>
            {config.mainHeaderTexts.map((entry, i) => (
              <p key={i} style={{ marginTop: ".5rem" }}>
                {parse(entry[locale || "en"])}
              </p>
            ))}
          </Grid.Col>
        </MediaQuery>
        <MediaQuery smallerThan="md" styles={{ display: "none" }}>
          <Grid.Col md={6} xs={12}>
            <Card
              shadow="md"
              style={{
                width: "100%",
                paddingTop: "100%",
                position: "relative",
              }}
              withBorder
            >
              <Box
                style={{
                  position: "absolute",
                  top: 0,
                  left: 0,
                  right: 0,
                  bottom: 0,
                  display: "flex",
                  alignItems: "stretch",
                  flexDirection: "column",
                }}
              >
                <Card.Section style={{ flexGrow: 1, marginBottom: "20px" }}>
                  <Box
                    style={{
                      height: "100%",
                      background:
                        "repeating-linear-gradient(45deg, " +
                        stripeColor +
                        ", " +
                        stripeColor +
                        " 4px, " +
                        stripeColor2 +
                        " 4px, " +
                        stripeColor2 +
                        " 8px)",
                    }}
                  ></Box>
                </Card.Section>
                <Card.Section
                  style={{
                    marginLeft: "20px",
                    marginRight: "20px",
                    overflow: "auto",
                  }}
                >
                  {post && post.getNewestPost ? (
                    <BlogPost post={post.getNewestPost} />
                  ) : (
                    <p style={{ textAlign: "center" }}>{t("nothingHere")}</p>
                  )}
                </Card.Section>
                <Card.Section style={{ flexGrow: 1, marginTop: "20px" }}>
                  <Box
                    style={{
                      height: "100%",
                      background:
                        "repeating-linear-gradient(45deg, " +
                        stripeColor +
                        ", " +
                        stripeColor +
                        " 4px," +
                        stripeColor2 +
                        " 4px, " +
                        stripeColor2 +
                        " 8px)",
                    }}
                  ></Box>
                </Card.Section>
              </Box>
            </Card>
          </Grid.Col>
        </MediaQuery>
      </Grid>
      <a href="#beers">
        <ActionIcon
          style={{ marginLeft: "auto", marginRight: "auto" }}
          size="3rem"
        >
          <Icon icon={ICONS.DOWN} color={getAccentColor(theme)} size="2rem" />
        </ActionIcon>
      </a>
    </div>
  );
}
