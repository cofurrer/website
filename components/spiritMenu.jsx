import { useState } from "react";

import { Button, Grid, Space, TextInput, useMantineTheme } from "@mantine/core";

import { useDebouncedState } from "@mantine/hooks";

import SpiritCard from "../components/spiritCard";
import AddSpiritModal from "../components/addSpiritModal";

import hasAccess from "../utilities/hasAccess";
import { getIconColor } from "../utilities/colors";

import { useTranslation } from "next-i18next";
import { useSession } from "next-auth/react";

import { Icon, ICONS } from "vseth-canine-ui";

import { gql, useQuery, useMutation } from "@apollo/client";

const updateAvailabilityMutation = gql`
  mutation updateSpiritAvailability($id: Int, $status: Boolean) {
    updateSpiritAvailability(id: $id, status: $status)
  }
`;

export default function SpiritMenu({ spirits, refetch, isArchive = false }) {
  const [updateAvailability] = useMutation(updateAvailabilityMutation);

  const [selection, setSelection] = useState(null);
  const [modalOpen, setModalOpen] = useState(false);
  const [filter, setFilter] = useDebouncedState("", 200);

  const { data: session } = useSession();
  const { t } = useTranslation("common");

  const theme = useMantineTheme();

  const edit = (spirit) => {
    setSelection(spirit);
    setModalOpen(true);
  };

  const add = () => {
    setSelection(null);
    setModalOpen(true);
  };

  const updateAvail = async (id, status) => {
    const res = await updateAvailability({
      variables: {
        id,
        status: !status,
      },
    });
    if (res.data.updateSpiritAvailability) refetch();
  };

  const matchesFilter = (spirit) => {
    const combinedString =
      spirit.name.toLowerCase() +
      " " +
      spirit.type.toLowerCase() +
      " " +
      spirit.tags.map((tag) => tag.tag.name.toLowerCase()).join(" ");
    return combinedString.includes(filter.toLowerCase());
  };

  return (
    <>
      <div style={{ textAlign: "center" }}>
        <h1>{t("spirits")}</h1>
        {hasAccess(session, true) && !isArchive && (
          <>
            <Button
              leftIcon={<Icon icon={ICONS.PLUS} color="white" />}
              onClick={add}
            >
              {t("addSpirit")}
            </Button>
            <Space h="md" />
          </>
        )}
      </div>

      <TextInput
        defaultValue={filter}
        onChange={(e) => setFilter(e.target.value)}
        icon={<Icon icon={ICONS.SEARCH} color={getIconColor(theme)} />}
        placeholder={t("search")}
        size="md"
      />

      <Space h="md" />
      <Grid>
        {spirits &&
          spirits
            .filter(
              (spirit) =>
                (spirit.isAvailable || hasAccess(session, true)) &&
                matchesFilter(spirit)
            )
            .map((spirit) => (
              <Grid.Col
                xs={12}
                sm={6}
                md={4}
                key={spirit.id}
                style={{ display: "flex" }}
              >
                <SpiritCard
                  spirit={spirit}
                  editSpirit={edit}
                  updateAvailability={updateAvail}
                  refetch={refetch}
                  isArchive={isArchive}
                />
              </Grid.Col>
            ))}
      </Grid>

      <AddSpiritModal
        open={modalOpen}
        close={() => setModalOpen(false)}
        spirit={selection}
        refetch={refetch}
      />
    </>
  );
}
