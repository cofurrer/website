-- DropForeignKey
ALTER TABLE `CocktailsOnIngredients` DROP FOREIGN KEY `CocktailsOnIngredients_cocktailId_fkey`;

-- DropForeignKey
ALTER TABLE `CocktailsOnTags` DROP FOREIGN KEY `CocktailsOnTags_cocktailId_fkey`;

-- AddForeignKey
ALTER TABLE `CocktailsOnIngredients` ADD CONSTRAINT `CocktailsOnIngredients_cocktailId_fkey` FOREIGN KEY (`cocktailId`) REFERENCES `Cocktail`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `CocktailsOnTags` ADD CONSTRAINT `CocktailsOnTags_cocktailId_fkey` FOREIGN KEY (`cocktailId`) REFERENCES `Cocktail`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
