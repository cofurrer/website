-- CreateTable
CREATE TABLE `Spirit` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(191) NOT NULL,
    `type` VARCHAR(191) NOT NULL,
    `price` DECIMAL(65, 30) NOT NULL,
    `alcohol` DECIMAL(65, 30) NOT NULL,
    `isAvailable` BOOLEAN NOT NULL DEFAULT false,
    `isArchived` BOOLEAN NOT NULL DEFAULT false,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `SpiritTag` (
    `name` VARCHAR(191) NOT NULL,

    UNIQUE INDEX `SpiritTag_name_key`(`name`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `SpiritsOnTags` (
    `spiritId` INTEGER NOT NULL,
    `tagName` VARCHAR(191) NOT NULL,

    PRIMARY KEY (`spiritId`, `tagName`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `SpiritsOnTags` ADD CONSTRAINT `SpiritsOnTags_spiritId_fkey` FOREIGN KEY (`spiritId`) REFERENCES `Spirit`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `SpiritsOnTags` ADD CONSTRAINT `SpiritsOnTags_tagName_fkey` FOREIGN KEY (`tagName`) REFERENCES `SpiritTag`(`name`) ON DELETE RESTRICT ON UPDATE CASCADE;
