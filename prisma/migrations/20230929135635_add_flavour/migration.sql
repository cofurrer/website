-- CreateTable
CREATE TABLE `Flavour` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `beerId` INTEGER NOT NULL,
    `userSub` VARCHAR(191) NOT NULL,
    `sweet` DECIMAL(65, 30) NOT NULL,
    `sour` DECIMAL(65, 30) NOT NULL,
    `hoppy` DECIMAL(65, 30) NOT NULL,
    `malty` DECIMAL(65, 30) NOT NULL,
    `fruity` DECIMAL(65, 30) NOT NULL,
    `bitter` DECIMAL(65, 30) NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `Flavour` ADD CONSTRAINT `Flavour_beerId_fkey` FOREIGN KEY (`beerId`) REFERENCES `Beer`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Flavour` ADD CONSTRAINT `Flavour_userSub_fkey` FOREIGN KEY (`userSub`) REFERENCES `User`(`sub`) ON DELETE CASCADE ON UPDATE CASCADE;
