/*
  Warnings:

  - The primary key for the `CocktailsOnIngredients` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to drop the column `ingredientId` on the `CocktailsOnIngredients` table. All the data in the column will be lost.
  - The primary key for the `Ingredient` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to drop the column `id` on the `Ingredient` table. All the data in the column will be lost.
  - A unique constraint covering the columns `[name]` on the table `Ingredient` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `ingredientName` to the `CocktailsOnIngredients` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE `CocktailsOnIngredients` DROP FOREIGN KEY `CocktailsOnIngredients_ingredientId_fkey`;

-- AlterTable
ALTER TABLE `CocktailsOnIngredients` DROP PRIMARY KEY,
    DROP COLUMN `ingredientId`,
    ADD COLUMN `ingredientName` VARCHAR(191) NOT NULL,
    ADD PRIMARY KEY (`cocktailId`, `ingredientName`);

-- AlterTable
ALTER TABLE `Ingredient` DROP PRIMARY KEY,
    DROP COLUMN `id`;

-- CreateIndex
CREATE UNIQUE INDEX `Ingredient_name_key` ON `Ingredient`(`name`);

-- AddForeignKey
ALTER TABLE `CocktailsOnIngredients` ADD CONSTRAINT `CocktailsOnIngredients_ingredientName_fkey` FOREIGN KEY (`ingredientName`) REFERENCES `Ingredient`(`name`) ON DELETE RESTRICT ON UPDATE CASCADE;
