/*
  Warnings:

  - The primary key for the `Tag` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to drop the column `cocktailId` on the `Tag` table. All the data in the column will be lost.
  - You are about to drop the column `id` on the `Tag` table. All the data in the column will be lost.
  - A unique constraint covering the columns `[name]` on the table `Tag` will be added. If there are existing duplicate values, this will fail.

*/
-- DropForeignKey
ALTER TABLE `Tag` DROP FOREIGN KEY `Tag_cocktailId_fkey`;

-- AlterTable
ALTER TABLE `Tag` DROP PRIMARY KEY,
    DROP COLUMN `cocktailId`,
    DROP COLUMN `id`;

-- CreateTable
CREATE TABLE `CocktailsOnTags` (
    `cocktailId` INTEGER NOT NULL,
    `tagName` VARCHAR(191) NOT NULL,

    PRIMARY KEY (`cocktailId`, `tagName`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateIndex
CREATE UNIQUE INDEX `Tag_name_key` ON `Tag`(`name`);

-- AddForeignKey
ALTER TABLE `CocktailsOnTags` ADD CONSTRAINT `CocktailsOnTags_cocktailId_fkey` FOREIGN KEY (`cocktailId`) REFERENCES `Cocktail`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `CocktailsOnTags` ADD CONSTRAINT `CocktailsOnTags_tagName_fkey` FOREIGN KEY (`tagName`) REFERENCES `Tag`(`name`) ON DELETE RESTRICT ON UPDATE CASCADE;
