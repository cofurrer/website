/*
  Warnings:

  - You are about to drop the column `submitter` on the `Comment` table. All the data in the column will be lost.
  - Added the required column `submitterSub` to the `Comment` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `Comment` DROP COLUMN `submitter`,
    ADD COLUMN `submitterSub` VARCHAR(191) NOT NULL;

-- CreateTable
CREATE TABLE `User` (
    `sub` VARCHAR(191) NOT NULL,

    PRIMARY KEY (`sub`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `Comment` ADD CONSTRAINT `Comment_submitterSub_fkey` FOREIGN KEY (`submitterSub`) REFERENCES `User`(`sub`) ON DELETE CASCADE ON UPDATE CASCADE;
